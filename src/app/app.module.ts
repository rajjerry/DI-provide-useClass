import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { UseClassComponent } from './use-class/use-class.component';
import { AnimalService } from './use-class/animal-service';
import { LionService } from './use-class/lion-service';
import { UseExistingComponent } from './use-existing/use-existing.component';
import { DesktopService } from './use-existing/desktop-service';
import { LaptopService } from './use-existing/laptop-service';
import { UseValueComponent } from './use-value/use-value.component';

import { AppComponent } from './app.component';


@NgModule({
  declarations: [
    AppComponent,
    UseClassComponent,
    UseExistingComponent,
    UseValueComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [
    { provide: AnimalService, useClass: LionService },
    // DesktopService,    // This is old  service
    { provide: DesktopService, useClass: LaptopService },   // Now I want to use New service.
  ],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
