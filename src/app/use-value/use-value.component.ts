import { Component, OnInit, InjectionToken, Inject } from '@angular/core';
import { Book } from './book';

const Angular_BOOK = new Book('Learning Provider Injection', 'Angular');
export const HELLO_MESSAGE = new InjectionToken<string>('Hello!');

@Component({
  selector: 'app-use-value',
  template: `
  <p>Book Name: <b>{{book.name}}</b> </p>
  <p>Category: <b>{{book.category}}</b></p>
  <p>Message: <b>{{message}}</b> </p>
`,
  providers: [
    { provide: Book, useValue: Angular_BOOK },
    { provide: HELLO_MESSAGE, useValue: 'Hello World!' }
  ]
})

export class UseValueComponent implements OnInit {

  constructor(private book: Book,
    @Inject(HELLO_MESSAGE) private message: string) { }

  ngOnInit() {
  }

}
