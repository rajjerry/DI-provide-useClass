import { Injectable } from '@angular/core';
import { Computer } from './IComputer';

@Injectable()
export class LaptopService implements Computer {
    getComputerName() {
        return 'LAPTOP';
    }
}
