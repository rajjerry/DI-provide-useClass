import { Component, OnInit } from '@angular/core';
import { DesktopService } from './desktop-service';
import { LaptopService } from './laptop-service';

@Component({
  selector: 'app-use-existing',
  template: `
  <h3> I work on {{computerName}} </h3>
`
})
export class UseExistingComponent implements OnInit {

  computerName: string;

  constructor(private computerService: DesktopService) { }

  ngOnInit() {
    this.computerName = this.computerService.getComputerName();
  }
}
