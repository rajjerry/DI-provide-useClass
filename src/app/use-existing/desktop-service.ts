import { Injectable } from '@angular/core';
import { Computer } from './IComputer';

@Injectable()
export class DesktopService implements Computer {
    getComputerName() {
        return 'Desktop';
    }
}
