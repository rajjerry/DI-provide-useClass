import { Component, OnInit } from '@angular/core';
import { AnimalService } from './animal-service';

@Component({
  selector: 'app-use-class',
  template: `
  <h3> {{name}} eats {{food}} </h3>
`
})
export class UseClassComponent implements OnInit {
  name: string;
  food: string;
  constructor(private animalService: AnimalService) { }

  ngOnInit() {
    this.name = this.animalService.getName();
    this.food = this.animalService.getFood();
  }

}
