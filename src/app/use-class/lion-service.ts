import { Injectable } from '@angular/core';
import { AnimalService } from './animal-service';

@Injectable()
export class LionService extends AnimalService {
    name = 'Lion';
    food = 'Meat';
}


// I can create another service like CowService and I have extend a service to do DI useClass.